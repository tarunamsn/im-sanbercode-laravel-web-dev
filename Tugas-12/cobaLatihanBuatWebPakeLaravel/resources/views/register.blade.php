<html>

<head>
    <title>SanberBook - Buat Akun Baru</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="namdep" /><br><br>

        <label>Last name:</label><br><br>
        <input type="text" name="nambel" /><br><br>

        <label>Gender:</label><br><br>
        <div class="control">
            <label class="radio">
                <input type="radio" name="answer">
                Male
            </label><br>
            <label class="radio">
                <input type="radio" name="answer">
                Female
            </label><br>
            <label class="radio">
                <input type="radio" name="answer">
                Other
            </label><br><br>
        </div>

        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="ID">Indonesian</option>
            <option value="SG">Singaporean</option>
            <option value="MY">Malaysian</option>
            <option value="AU">Australian</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language" />Bahasa Indonesia<br>
        <input type="checkbox" name="language" />English<br>
        <input type="checkbox" name="language" />Other<br><br>

        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="Sign Up" />
    </form>
</body>

</html>