<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function welcome(Request $request) {
        $firstname = $request['namdep'];
        $lastname = $request['nambel'];

        return view('welcome', ['firstname' => strtoupper($firstname), 'lastname' => strtoupper($lastname)]);
    }
}
