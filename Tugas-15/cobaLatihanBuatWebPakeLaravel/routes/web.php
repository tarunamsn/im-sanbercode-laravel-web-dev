<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/table', function () {
    return view('pages.table');
});

Route::get('/data-tables', function () {
    return view('pages.data-tables');
});

//CRUD STARTS HERE

//CREATE
Route::get('/cast/create', [CastController::class, 'create']); //THIS IS TO ADD MOVIE CAST DATAS
Route::post('/cast', [CastController::class, 'store']); //THIS IS TO SEND MOVIE CAST DATAS TO DB

//READ
Route::get('/cast', [CastController::class, 'index']); //THIS IS TO SHOW ALL MOVIE CAST DATAS IN DB
Route::get('/cast/{cast_id}', [CastController::class, 'show']); //THIS IS TO SHOW SOME OF MOVIE CAST DATAS IN DB BY ID

//UPDATE
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']); //THIS IS TO EDIT SOME OF MOVIE CAST DATAS IN DB
Route::put('/cast/{cast_id}', [CastController::class, 'update']); //THIS IS TO SHOW SOME OF MOVIE CAST DATAS THAT HAS BEEN EDITED

//DELETE
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']); //THIS IS TO DELETE SOME OF MOVIE CAST DATAS IN DB