@extends('layouts.masters')

@section('title')
Show Movie Cast Infos
@endsection

@section('card-title')
Show Movie Cast Infos
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm">Add</a>
<table class="table">
    <caption>List of Movie Casts</caption>
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value -> name}}</td>
                <td>{{$value -> age}}</td>
                <td>{{$value -> bio}}</td>
                <td>
                    <form action="/cast/{{$value -> id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$value -> id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$value -> id}}/edit" class="btn btn-primary btn-sm">Update</a>
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>NO SUCH DATA AVAILABLE</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection
