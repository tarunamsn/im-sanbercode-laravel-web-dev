@extends('layouts.masters')

@section('title')
Show Movie Cast Info in Detail
@endsection

@section('card-title')
Show Movie Cast Info in Detail
@endsection

@section('content')
<h1>{{$cast -> name}}</h1>
<p>Age: {{$cast -> age}}</p>
<p>{{$cast -> bio}}</p>
<a href="/cast" class="btn btn-primary btn-sm">Back</a>
@endsection
