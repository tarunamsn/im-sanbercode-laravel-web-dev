@extends('layouts.masters')

@section('title')
Edit Movie Casts
@endsection

@section('card-title')
Edit Movie Casts
@endsection

@section('content')
<form action="/cast/{{$cast -> id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Name</label>
      <input type="text" class="form-control" name="name" value="{{$cast -> name}}"
      placeholder="Type the name of the Actor/Actress here...">
    </div>
    @error('name')
        <div class="alert alert-danger">NAME CANNOT BE EMPTY!</div>
    @enderror
    <div class="form-group">
      <label>Age</label>
      <input type="text" class="form-control" name="age"
      value="{{$cast -> age}}" placeholder="Type the age of the Actor/Actress here...">
    </div>
    @error('age')
        @if ('age' != 'numeric')
            <div class="alert alert-danger">AGE MUST BE NUMBER!</div>
        @else
            <div class="alert alert-danger">AGE CANNOT BE EMPTY!</div>
        @endif
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea class="form-control" cols="30" rows="10" name="bio" placeholder="Type the bio of the Actor/Actress here...">
    {{$cast -> bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">BIO CANNOT BE EMPTY!</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
