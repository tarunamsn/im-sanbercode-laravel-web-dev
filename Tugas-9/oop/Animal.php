<?php
class Animal {
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    function __construct($name) {
        $this->name = $name; 
    }
    
    function set_legs($legs) {
        $this->legs = $legs;
    }
    function get_legs() {
        return $this->legs;
    }
    
    function set_cold_blooded($cold_blooded) {
        $this->cold_blooded = $cold_blooded;
    }
    function get_cold_blooded() {
        return $this->cold_blooded;
    }
}
?>